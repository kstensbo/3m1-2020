fname = solutions

solutions: fname=solutions
solutions: document

notes: fname=notes
notes: document

qsolutions: fname=solutions
qsolutions: quick

qnotes: fname=notes
qnotes: quick

default: document

document.pdf: .FORCE #cv
	pdflatex $(fname).tex
	biber $(fname)
	pdflatex $(fname).tex
	pdflatex $(fname).tex

pdf:	document.pdf

document: pdf

.FORCE:

quick:
	pdflatex $(fname).tex

refs:
	bibtool -- delete.field{file} \
			-- delete.field{owner} \
			-- delete.field{timestamp} \
			-- delete.field{abstract} \
			-- delete.field{__markedentry} \
			-- delete.field{url} \
			-- delete.field{issn} \
			-- delete.field{month} \
			-x $(fname).aux -o refs.bib

clean:
	rm -f $(fname).pdf *.aux *.log *.toc *.thm *.out *.bbl *.blg *.lox *.run.xml *.bcf
