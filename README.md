# 3M1 2020

Notes and problem solutions for 3M1 2020.

For discussions of some of the concepts covered in 3M1, see [notes.pdf](notes.pdf). It currently contains discussion of the following topics or concepts:
  * Sampling and importance sampling.
  * Conjugate gradient method.
  * Rate of convergence.

For solutions to selected questions, see [solutions.pdf](solutions.pdf). It currently contains solutions to the following questions:
  * Examples paper 2
    * Question 7
  * Examples paper 3
    * Question 4
  * Examples paper 4
    * Question 3
    * Question 7

More to come!

## Additional resources

### Barrier and penalty functions
To get a more intuitive feeling for the different solutions that can be obtained with barrier and penalty functions, have a look at this interactive visualisation: [Barrier and penalty functions](https://www.geogebra.org/m/pdhetcew)

It shows an objective function, $`f(x)`$ (in blue), which we want to minimise subject to $`x \leq 3`$. The constraint is turned into $`g(x) = x - 3`$ (consistent with the notation in the handouts), and three augmented objective functions are defined: $`q_1`$ is using a penalty function, $`q_2`$ is using an inverse barrier function, and $`q_3`$ is using a logarithmic barrier function. Each $`q_i`$ has an associated point $`M_i`$ marking the minimum of the function. You can use the pretty slider to adjust $`\kappa`$ (on a log scale) and see how the three found minima change based on the value.

In particular, notice the effect described in the handouts where the penalty function can end up finding an unfeasible solution.

